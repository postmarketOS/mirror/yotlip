//go:build mobian

package distro

import (
	"embed"
	"gitlab.com/postmarketOS/yotlip/config"
)

type Mobian struct {
	Distro
	ReleasesPopulator
}

func New(c *config.Config, res *embed.FS) (*Mobian, error) {
	// TODO: don't take in embed.FS..

	d, err := initDistro("mobian", c, res)
	if err != nil {
		return nil, err
	}
	return &Mobian{Distro: d}, nil
}

func (m *Mobian) PopulateReleases(cacheDir string) error {
	// custom implementation

	return nil
}
