//go:build linux

package flasher

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/godbus/dbus/v5"
	"gitlab.com/postmarketOS/yotlip/pkgs/fastcopy"
	"gitlab.com/postmarketOS/yotlip/pkgs/progress"
)

// Uses UDisks2 via dbus to open a disk (relying on polkit through UDisks2 for
// authentication), and flashes it. Progress is reported as a percentage of 1.0
// to the given channel
func (r *RawFlasher) Flash(imagePath string, prog chan progress.Progress) {
	defer close(prog)

	img, err := os.Open(imagePath)
	if err != nil {
		err = fmt.Errorf("flasher/linux_raw.Flash: unable to open image: %s: %w", imagePath, err)
		prog <- progress.Progress{Err: err}
		return
	}
	defer img.Close()

	// file size, used for reporting progress
	stat, err := img.Stat()
	if err != nil {
		return
	}
	imgSize := uint64(stat.Size())

	// this expects a 'disk' in the form /dev/xyz
	// TODO: use uuids? would require more dbus calls to map the uuid to the
	// right dbus path... so maybe not worth it.
	disk := filepath.Base(r.Disk)

	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		err = fmt.Errorf("flasher/linux_raw.Flash: unable to open connection to DBus system bus: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	defer conn.Close()

	if !conn.SupportsUnixFDs() {
		err = fmt.Errorf("flasher/linux_raw.Flash: system does not support unix file descriptors: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}

	diskO := conn.Object("org.freedesktop.UDisks2", dbus.ObjectPath("/org/freedesktop/UDisks2/block_devices/"+disk))

	var fd dbus.UnixFD
	options := map[string]dbus.Variant{"flags": dbus.MakeVariant(os.O_SYNC)}
	err = diskO.Call("org.freedesktop.UDisks2.Block.OpenDevice", 0, "w", options).Store(&fd)
	if err != nil {
		err = fmt.Errorf("flasher/linux_raw.Flash: unable to open block device: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	f := os.NewFile(uintptr(fd), r.Disk)
	defer f.Close()

	counter := &progress.Counter{
		Progress: prog,
		Total:    imgSize,
	}

	_, err = fastcopy.Copy(f, io.TeeReader(img, counter))
	if err != nil {
		err = fmt.Errorf("flasher/linux_raw.Flash: failure writing to disk: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
}
