package device

import (
	"gioui.org/layout"
	"image"
)

type (
	C = layout.Context
	D = layout.Dimensions
)

type Device struct {
	Key         string // e.g. "pine64_pinephone"
	DisplayName string // e.g. "Pine64 Pinephone"
	Img         image.Image
	Visible     bool
}
