package about

import (
	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gitlab.com/postmarketOS/yotlip/core"
)

type Page struct {
	app        *core.App
	backButton widget.Clickable
	err        error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

func New(app *core.App) *Page {
	return &Page{app: app}
}

func (p *Page) Update(data interface{}) bool {
	// stub
	return false
}

var thirdPartyLicenses = []string{
	"postmarketOS, CC BY-SA 4.0",
	"Purism, CC BY-SA 4.0",
	"Mobian, CC BY-SA 4.0",
}

func (p *Page) Layout(gtx C) D {
	// process any button clicks on page
	if p.backButton.Clicked() {
		p.app.Router.Pop()
	}

	sections := []layout.FlexChild{
		layout.Rigid(material.H3(p.app.Theme, "Yotlip").Layout),
		layout.Rigid(material.Body1(p.app.Theme, "gitlab.com/postmarketOS/yotlip").Layout),
		layout.Rigid(material.Body1(p.app.Theme, "Copyright © 2021 Clayton Craft, GPL-3-or-later").Layout),
		layout.Rigid(func(gtx C) D {
			return layout.Inset{Top: unit.Dp(20)}.Layout(gtx, material.H6(p.app.Theme, "Additional licenses for resources in this application:").Layout)
		}),
	}

	// 3rd party licenses
	for _, s := range thirdPartyLicenses {
		sections = append(sections,
			layout.Rigid(material.Body1(p.app.Theme, s).Layout),
		)
	}

	// back button
	sections = append(sections,
		layout.Flexed(100, func(gtx C) D {
			return layout.SW.Layout(gtx, func(gtx C) D {
				gtx.Constraints.Min.X = 75
				gtx.Constraints.Max.X = 75
				return layout.Inset{Bottom: unit.Dp(10), Top: unit.Dp(20)}.Layout(gtx, material.Button(p.app.Theme, &p.backButton, "<- Back").Layout)
			})
		}),
	)

	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx, sections...)
	})
}
