package select_device

import (
	"fmt"
	"image/color"
	"log"
	"strings"

	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gitlab.com/postmarketOS/yotlip/core"
	"gitlab.com/postmarketOS/yotlip/device"
	"gitlab.com/postmarketOS/yotlip/pages/select_release"
	"gitlab.com/postmarketOS/yotlip/ui"
)

type Page struct {
	app           *core.App
	deviceButtons []*deviceButton
	header        *ui.Header
	footer        *ui.Footer
	scrollWrap    *ui.ScrollWrap
	searchBox     *widget.Editor
	err           error
}

type (
	C = layout.Context
	D = layout.Dimensions
)

type deviceButton struct {
	ui.ImageButton
	device  *device.Device
	visible bool
}

func New(app *core.App) *Page {
	p := &Page{
		app:        app,
		header:     ui.NewHeader(app),
		footer:     ui.NewFooter(app),
		scrollWrap: &ui.ScrollWrap{Theme: app.Theme},
		searchBox:  &widget.Editor{SingleLine: true},
	}

	// initial focus on the text box saves users 1 click!
	p.searchBox.Focus()

	return p
}

func (p *Page) Update(data interface{}) bool {
	// stub
	return false
}

func (p *Page) layoutSearchBox(gtx C) D {
	s := material.Editor(p.app.Theme, p.searchBox, "Type to begin searching")
	s.Font.Style = text.Italic
	border := widget.Border{Color: color.NRGBA{A: 0xDD}, CornerRadius: unit.Dp(8), Width: unit.Px(2)}
	return layout.UniformInset(unit.Dp(8)).Layout(gtx, func(gtx C) D {
		return border.Layout(gtx, func(gtx C) D {
			return layout.UniformInset(unit.Dp(8)).Layout(gtx, s.Layout)
		})
	})
}

// Layout the left-hand side (LHS) of the page body
func (p *Page) layoutLHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,

			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(8)}.Layout(gtx,
					material.H5(p.app.Theme, "1. Select a device:").Layout)
			}),
			// search box layout
			layout.Rigid(func(gtx C) D {
				return layout.Inset{Bottom: unit.Dp(4)}.Layout(gtx, p.layoutSearchBox)
			}),
			// list of devices
			layout.Flexed(100, func(gtx C) D {
				var children []func(gtx C) D
				for _, c := range p.deviceButtons {
					// skip hidden/filtered devices
					if !c.visible {
						continue
					}
					c := c
					children = append(children, func(gtx C) D {
						return c.Layout(gtx, p.app.Theme)
					})
				}
				p.scrollWrap.Items = children
				return p.scrollWrap.Layout(gtx)
			}),
		)
	})
}

// Layout the right-hand side (RHS) of the page body
func (p *Page) layoutRHS(gtx C) D {
	return layout.UniformInset(unit.Dp(20)).Layout(gtx, func(gtx C) D {
		return layout.Flex{
			Axis:    layout.Vertical,
			Spacing: layout.SpaceEnd,
		}.Layout(gtx,
			// TODO: improve this text, and put strings like this in some central location?
			layout.Flexed(100, material.Label(p.app.Theme, unit.Dp(18), "The devices here are supported by "+p.app.Distro.GetName()+". Find your device in the list and click on it to proceed.").Layout),
		)
	})
}

func (p *Page) Layout(gtx C) D {
	if len(p.app.Distro.SupportedDevices()) == 0 {
		p.err = fmt.Errorf("select_device: no supported devices found")
	}

	if len(p.deviceButtons) == 0 {
		// if a valid device was given on the cmdline, use it
		// this check is done here so that it happens only once instead of on
		// every frame redraw
		if *p.app.Options["device"] != "" {
			var dev *device.Device
			for _, d := range p.app.Devices {
				if d.Key == *p.app.Options["device"] {
					dev = d
					break
				}
			}
			if dev != nil {
				log.Printf("pages/select_device: auto-selecting device from commandline: %q", *p.app.Options["device"])
				p.app.Router.Pages["select_release"] = select_release.New(p.app, dev)
				p.app.Router.Push("select_release")
				p.app.Window.Invalidate()
			} else {
				log.Printf("pages/select_device: ignoring unsupported/unknown device set on commandline: %q", *p.app.Options["device"])
			}
		}

		// generate list of device buttons
		for _, device := range p.app.Devices {
			if !p.app.Distro.DeviceSupported(device.Key) {
				log.Printf("pages/select_device: skipping unsupported device : %q", device.Key)
				continue
			}
			db := deviceButton{device: device}
			db.Text = device.DisplayName
			db.Img = device.Img
			button := ui.Button{X: 100, Y: 100, ButtonTemplate: &db}
			db.Button = button
			db.visible = true

			p.deviceButtons = append(p.deviceButtons, &db)
		}

	}

	for _, e := range p.searchBox.Events() {
		if _, ok := e.(widget.ChangeEvent); ok {
			// filter devices based on text entered
			t := strings.ToLower(p.searchBox.Text())
			for _, b := range p.deviceButtons {
				if t == "" {
					b.visible = true
				} else {
					b.visible = strings.Contains(strings.ToLower(b.Text), t)
				}
			}
		}
	}

	// process any button clicks on page
	for _, button := range p.deviceButtons {
		if button.Clicked() {
			p.app.Router.Pages["select_release"] = select_release.New(p.app, button.device)
			p.app.Router.Push("select_release")
			p.app.Window.Invalidate()
			break
		}
	}
	sections := []layout.FlexChild{
		// Header
		layout.Rigid(func(gtx C) D {
			return p.header.Layout(gtx, p.app.Theme)
		}),
	}

	if p.err != nil {
		sections = append(sections, layout.Flexed(100, func(gtx C) D {
			return ui.ErrorLayout(gtx, p.app.Theme, p.err)
		}))
	} else {
		sections = append(sections, []layout.FlexChild{
			layout.Rigid(func(gtx C) D {
				return layout.Flex{
					Axis:    layout.Horizontal,
					Spacing: layout.SpaceBetween,
				}.Layout(gtx,
					// UI content left hand side
					layout.Flexed(60, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutLHS)
					}),
					// UI content right hand side
					layout.Flexed(40, func(gtx C) D {
						return widget.Border{
							Color: color.NRGBA{A: 107},
							Width: unit.Dp(0.5),
						}.Layout(gtx, p.layoutRHS)
					}),
				)
			}),
			// footer
			layout.Flexed(100, func(gtx C) D {
				return p.footer.Layout(gtx, p.app.Theme)
			}),
		}...)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceEnd,
	}.Layout(gtx, sections...)
}
