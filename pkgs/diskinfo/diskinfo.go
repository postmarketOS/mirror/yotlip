package diskinfo

type DiskInfo struct {
	Vendor     string
	Model      string
	SizeGiB    float32
	DeviceFile string // e.g. /dev/sdb
}
