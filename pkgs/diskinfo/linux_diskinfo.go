//go:build linux

package diskinfo

import (
	"bytes"
	"fmt"
	"github.com/godbus/dbus/v5"
	"log"
	"strings"
)

func GetDisks(includePermanentDisks bool) ([]*DiskInfo, error) {
	var d []*DiskInfo

	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		return nil, fmt.Errorf("diskinfo.GetDisks: unable to open connection to DBus system bus: %w", err)
	}
	defer conn.Close()

	// get a list of all block devices
	diskMgr := conn.Object("org.freedesktop.UDisks2", dbus.ObjectPath("/org/freedesktop/UDisks2/Manager"))
	options := map[string]dbus.Variant{"flags": dbus.MakeVariant("")}
	var disks []dbus.ObjectPath
	err = diskMgr.Call("org.freedesktop.UDisks2.Manager.GetBlockDevices", 0, options).Store(&disks)
	if err != nil {
		return nil, fmt.Errorf("diskinfo.GetDisks: unable to get disks: %w", err)
	}

	for _, diskPath := range disks {
		disk := conn.Object("org.freedesktop.UDisks2", dbus.ObjectPath(diskPath))

		// ignore block devices that are partitions
		if s := isPartition(disk); s {
			log.Printf("diskinfo.GetDisks: skip: block device seems to be a partition: %s", diskPath)
			continue
		}

		// get reference to disk @ org.fd.UDisks2.Block.Drive property
		var drivePath string
		if v, err := disk.GetProperty("org.freedesktop.UDisks2.Block.Drive"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get drive info: %s", err)
			continue
		} else {
			v.Store(&drivePath)
		}

		if !strings.Contains(drivePath, "/drives/") {
			log.Printf("diskinfo.GetDisks: skip: drive path for %q doesn't seem valid: %s", diskPath, drivePath)
			continue

		}

		drive := conn.Object("org.freedesktop.UDisks2", dbus.ObjectPath(drivePath))

		var removable bool
		if v, err := drive.GetProperty("org.freedesktop.UDisks2.Drive.Removable"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get removable property: %s", drivePath)
			continue
		} else {
			v.Store(&removable)
		}
		if !removable && !includePermanentDisks {
			log.Printf("diskinfo.GetDisks: skip: nonremovable drive: %s", drivePath)
			continue
		}

		var vendor string
		if v, err := drive.GetProperty("org.freedesktop.UDisks2.Drive.Vendor"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get vendor property: %s", drivePath)
			continue
		} else {
			v.Store(&vendor)
		}

		var model string
		if v, err := drive.GetProperty("org.freedesktop.UDisks2.Drive.Model"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get model property: %s", drivePath)
			continue
		} else {
			v.Store(&model)
		}

		var size uint64
		if v, err := drive.GetProperty("org.freedesktop.UDisks2.Drive.Size"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get size property: %s", drivePath)
			continue
		} else {
			v.Store(&size)
		}

		var deviceFile string
		if v, err := disk.GetProperty("org.freedesktop.UDisks2.Block.Device"); err != nil {
			log.Printf("diskinfo.GetDisks: unable to get block property: %s", diskPath)
			log.Print(err)
			continue
		} else {
			// for some reason the signature of this property is a byte array...
			var b []byte
			v.Store(&b)
			// sometimes a null byte is in the output...
			deviceFile = string(bytes.Trim(b[:], "\x00"))
		}

		d = append(d, &DiskInfo{
			Vendor:     vendor,
			Model:      model,
			SizeGiB:    float32(size) / 1000000000.0,
			DeviceFile: deviceFile,
		})
		log.Printf("diskinfo.GetDisks: found disk: %s %s (%s)", vendor, model, deviceFile)
	}

	return d, nil
}

// Determine if the disk is a partition or not
func isPartition(disk dbus.BusObject) bool {
	// getting this property from a non-partition fails with a dbus error, so if it
	// succeeds then it must(?) be a partition
	_, err := disk.GetProperty("org.freedesktop.UDisks2.Partition.UUID")
	return err == nil
}
