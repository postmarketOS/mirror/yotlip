package extractor

import (
	"fmt"
	"github.com/xi2/xz"
	"gitlab.com/postmarketOS/yotlip/pkgs/fastcopy"
	"gitlab.com/postmarketOS/yotlip/pkgs/progress"
	"io"
	"log"
	"os"
	"path/filepath"
)

type Archive struct {
	InFile  string
	OutFile string
}

func (a *Archive) Extract(prog chan progress.Progress) {
	defer close(prog)

	fd, err := os.Open(a.InFile)
	if err != nil {
		err = fmt.Errorf("Archive.Extract: unable to open file: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	defer fd.Close()

	var reader io.Reader
	// detecting file type based on extension is the simplest way to figure out
	// what the file is. there are other ways in Go, e.g. using some http
	// module, but they seem too complex, and it seems unlikely that the
	// extension is going to be 'wrong' for the file's actual type
	var size uint64
	switch filepath.Ext(a.InFile) {
	case ".xz":
		log.Print("Archive.Extract: image appears to be compressed with xz")
		reader, err = xz.NewReader(fd, 0)
		if err != nil {
			err = fmt.Errorf("DistroImage.Read: unable to read xz archive: %w", err)
			prog <- progress.Progress{Err: err}
			return
		}
		// getting uncompressed size of an xz archive is not possible with any current libraries
		size = 0
	default:
		log.Print("Archive.Extract: file does not have a supported extension, so assuming it is already extracted")
		// copy the file to the destination
		// TODO: find a better way to handle this case
		reader = fd
	}

	newFd, err := os.OpenFile(a.OutFile, os.O_CREATE|os.O_SYNC|os.O_WRONLY, 0644)
	if err != nil {
		err = fmt.Errorf("Archive.Extract: unable to open new file for writing: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}
	defer newFd.Close()

	// if extracted size is known, use a 'real' counter to report progress
	if size != 0 {
		counter := &progress.Counter{
			Progress: prog,
			Total:    size,
			// # what size to report?
		}
		if _, err := fastcopy.Copy(newFd, io.TeeReader(reader, counter)); err != nil {
			err = fmt.Errorf("Archive.Extract: unable to extract: %w", err)
			prog <- progress.Progress{Err: err}
			return
		}
		log.Printf("Archive.Extract: extracted file: %q", a.OutFile)
		return
	}

	// user a fake progress counter if size is unknown
	log.Printf("Archive.Extract: extracted file size is unknown, not reporting real progress")
	stopCounter := make(chan bool)
	counter := progress.FakeCounter{
		Stop:     stopCounter,
		Progress: prog,
	}
	go counter.Run()
	if _, err := fastcopy.Copy(newFd, reader); err != nil {
		err = fmt.Errorf("Archive.Extract: unable to extract: %w", err)
		prog <- progress.Progress{Err: err}
		return
	}

	stopCounter <- true

	log.Printf("Archive.Extract: extracted file: %q", a.OutFile)

	return
}
