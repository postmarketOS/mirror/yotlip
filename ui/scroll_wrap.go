package ui

import (
	"gioui.org/layout"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/outlay"
)

type ScrollWrap struct {
	Theme *material.Theme
	Items []func(gtx C) D
	list  widget.List
}

func (s *ScrollWrap) Layout(gtx C) D {
	// hold elements in a single-item list so it's scrollable if it wraps
	// below the window's visible area
	s.list.Axis = layout.Vertical

	return material.List(s.Theme, &s.list).Layout(gtx, 1, func(gtx C, _ int) D {
		hWrap := outlay.GridWrap{
			Axis:      layout.Horizontal,
			Alignment: layout.End,
		}
		return layout.Flex{
			Axis: layout.Horizontal,
		}.Layout(gtx,
			layout.Rigid(func(gtx C) D {
				return hWrap.Layout(gtx, len(s.Items), func(gtx C, i int) D {
					return s.Items[i](gtx)
				})
			}),
			// this is a filler that expands to the right, so that scrolling
			// with the pointer in this blank area will still scroll the list
			layout.Flexed(1, layout.Spacer{}.Layout),
		)
	})
}
