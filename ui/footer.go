package ui

import (
	"gioui.org/layout"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget/material"
	"gitlab.com/postmarketOS/yotlip/core"
	"image"
)

type Footer struct {
	app          *core.App
	th           *material.Theme
	distroLogo   image.Image
	distroLogoOp paint.ImageOp
	distroName   string
}

func NewFooter(app *core.App) *Footer {
	return &Footer{
		app:        app,
		th:         app.Theme,
		distroLogo: app.Distro.GetLogo(),
		distroName: app.Distro.GetName(),
	}
}

func (h *Footer) Layout(gtx C, th *material.Theme) D {
	return layout.Flex{
		Axis:      layout.Vertical,
		Alignment: layout.End,
		Spacing:   layout.SpaceStart,
	}.Layout(gtx,
		layout.Rigid(func(gtx C) D {
			// stub
			return layout.UniformInset(unit.Dp(0)).Layout(gtx, layout.Spacer{}.Layout)
		}),
	)
}
