package core

import (
	"embed"
	"flag"
	gioApp "gioui.org/app"
	"gioui.org/font/gofont"
	"gioui.org/widget/material"
	"git.sr.ht/~gioverse/skel/router"
	"git.sr.ht/~gioverse/skel/scheduler"
	"gitlab.com/postmarketOS/yotlip/config"
	"gitlab.com/postmarketOS/yotlip/device"
	"gitlab.com/postmarketOS/yotlip/distro"
	"gitlab.com/postmarketOS/yotlip/pkgs/util"
	"image"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type App struct {
	Config     config.Config
	distroLogo *image.Image
	Theme      *material.Theme
	Router     router.Router
	Bus        scheduler.Connection
	CacheDir   string
	Window     *gioApp.Window

	// holds options passed on the commandline, for use in pages
	Options map[string]*string

	Devices   []*device.Device
	Resources *embed.FS
	Distro    distro.DistroTemplate
	UIs       []*distro.DistroUI
}

func (a *App) loadImagesUI() error {
	var uiImages map[string]image.Image

	for _, d := range a.Distro.SupportedDevices() {
		for _, r := range a.Distro.ListReleases(d) {
			for _, u := range r.UIs {
				_, ok := uiImages[u.Name]
				if ok {
					continue
				}
				_, ok = a.Config.UIs[u.Name]
				if !ok {
					log.Printf("core.loadImagesUI: skipping UI not found in config: %q", u.Name)
					continue
				}
				img, err := util.LoadImageFromRes("imgs/"+a.Config.UIs[u.Name].Photo, a.Resources)
				if err != nil {
					return err
				}
				u.Photo = img
			}
		}
	}

	return nil
}

func NewApp(w *gioApp.Window, config config.Config, res *embed.FS, bus scheduler.Connection) (App, error) {
	th := material.NewTheme(gofont.Collection())

	app := App{
		Config:    config,
		Resources: res,
		Theme:     th,
		Bus:       bus,
		Window:    w,
		Options: map[string]*string{
			"device":  flag.String("device", "", "Device to pre-select, in the form: <manufacturer>-<codename>. E.g. 'pine64-pinephone'"),
			"ui":      flag.String("ui", "", "UI to pre-select."),
			"release": flag.String("release", "", "Distro release to pre-select."),
			"disk":    flag.String("disk", "", "For devices that use the 'raw' flasher, flash to the specified disk. E.g. '/dev/sdb'. ***WARNING*** This is dangerous."),
		},
	}
	flag.Parse()

	dist, err := distro.New(&config, app.Resources)
	if err != nil {
		log.Print("core.NewApp: unable to init new distro object")
		return app, err
	}
	app.Distro = dist

	appName := strings.ToLower(app.Distro.GetName()) + "_installer"
	cacheBaseDir, err := os.UserCacheDir()
	if err != nil {
		log.Print("core.NewApp: unable to get user cache directory")
		return app, err
	}

	app.CacheDir = filepath.Join(cacheBaseDir, appName)
	if err := os.MkdirAll(app.CacheDir, 0755); err != nil {
		log.Print("core.NewApp: unable to create app cache directory")
		return app, err
	}

	if err := app.Distro.PopulateReleases(app.CacheDir); err != nil {
		return app, err
	}

	if err := app.loadImagesUI(); err != nil {
		return app, err
	}

	// Only devices supported by the distro and with config
	if len(app.Devices) == 0 {
		for k, n := range config.Devices {
			if !app.Distro.DeviceSupported(k) {
				continue
			}

			d := device.Device{
				Key:         k,
				DisplayName: n.DisplayName,
			}

			if n.Photo == "" {
				n.Photo = "generic.png"
			}
			img, err := util.LoadImageFromRes("imgs/"+n.Photo, app.Resources)
			if err != nil {
				//TODO: don't Fatal out...
				log.Fatal(err)
			}

			d.Img = img
			app.Devices = append(app.Devices, &d)
		}
	}

	return app, nil
}
